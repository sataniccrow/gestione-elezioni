<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	include_once("./utils/Validator.php");
	$val = SecurityCheck::checkIdentity("admin");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	include_once("./php/header.php");

	require_once('./dao/generated/include_dao.php');

	if(isset($_GET['action'])){
		if($_GET['action'] == "add"){
			if(isset($_POST['userName']) && isset($_POST['surname']) && isset($_POST['name']) && isset($_POST['password']) && isset($_POST['role'])
				 && !empty($_POST['userName']) && !empty($_POST['surname']) && !empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['role'])){
				try{
					$transaction = new Transaction();
					$user = new User();

					$user->userName = $_POST['userName'];
					$user->name = $_POST['name'];
					$user->surname = $_POST['surname'];

					if($_POST['role'] === "regular" || $_POST['role'] === "admin"){
						$user->userType = $_POST['role'];
					}else{
						throw new Exception("Invalid role");
					}
					$user->password = Validator::createHash($_POST['password']);

					DAOFactory::getUserDAO()->insert($user);
					$transaction->commit();	
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}else{
				echo 'A mandatory parameter is missing';
			}
		}

		if($_GET['action'] == 'delete' && isset($_POST['userId'])){
			if($_POST['userId'] != $_SESSION['userId']){
				$transaction = new Transaction();
				DAOFactory::getUserDAO()->delete($_POST['userId']);
				$transaction->commit();	
			}else{
				echo 'You are trying to delete yourself!';
			}
			
		}else{
			if($_GET['action'] == 'modify' && isset($_POST['userId'])){
			
				$user = DAOFactory::getUserDAO()->load($_POST['userId']);

				if(isset($user)){
					$transaction = new Transaction();
					$user->surname = (isset($_POST['surname']) && $_POST['surname'] != '') ? $_POST['surname'] : $user->surname;
					$user->name = (isset($_POST['name']) && $_POST['name'] != '') ? $_POST['name'] : $user->name;
					$user->password = (isset($_POST['password']) && $_POST['password'] != '')? Validator::createHash($_POST['password']) : $user->password;

					//security check: an admin can lock everyone but himself
					if($_POST['userId'] != $_SESSION['userId']){
						$user->isBlocked = (isset($_POST['isBlocked']) && $_POST['isBlocked'] != '')? ($_POST['isBlocked']=='LOCKED')?1:0:0;
					}

					DAOFactory::getUserDAO()->update($user);
					$transaction->commit();
				}
			}
		}
	}
	
?>


<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./admin.php">ADMIN</a></div>
	<div class="nav_active">USER</div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>

<div class="table_row">
	<div class="box_item_center">
		<div class="table_wrapper">
			<div class="nano">
				<div class="content">
					<table>
						<thead>
							<tr>
								<th>Surname</th>
								<th>Name</th>
								<th>Username</th>
								<th>Role</th>
								<th>Office</th>
								<th>Start</th>
								<th>End</th>
								<th>isBlocked</th>
								<th>Delete</th>
								<th>Modify</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$userList = array();
								$userList = DAOFactory::getUserDAO()->queryAllOrderBy("surname");

								if(count($userList) > 0){
									foreach ($userList as $val){
										$office = DAOFactory::getOfficeDAO()->load($val->officeId);

										echo '<tr class="userData">';
										echo '<td class="surname">'.$val->surname."</td>";
										echo '<td class="name">'.$val->name.'</td>';
										echo '<td class="userName">'.$val->userName.'</td>';
										echo '<td class="userType">'.$val->userType.'</td>';
										echo '<td class="office">'.(isset($office)? $office->title : "").'</td>';
										echo '<td class="startingDate">'.((isset($val->officeId)&& !empty($val->officeId))?date("d/m/Y", strtotime($val->startingDate)):"")."</td>";
										echo '<td class="endingDate">'.((isset($val->officeId)&& !empty($val->officeId))?date("d/m/Y", strtotime($val->endingDate)):"").'</td>';
										echo '<td class="isBlocked">'.(($val->isBlocked == 0)? 'UNLOCKED':'<b>LOCKED</b>').'</td>';
										echo '<td>';
										echo '<img class="userDelete" src="./grapx/remove.png" alt="'.$val->userId.'" width="20px">';
										echo'</td>';
										echo '<td>';
										echo '<img class="userModify" src="./grapx/edit.png" alt="'.$val->userId.'" width="20px">';
										echo'</td>';
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="table_row_footer">
			<img src="./grapx/add.png" class="userAdd" alt="add new user" width="40px">
		</div>
	</div>
</div>

<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#userFormContainer").is(":visible")){
					// $( "#userForm" ).hide(400);
				}else{
					$( "#userFormContainer" ).show(400);
				}
			};

			$( ".userDelete" ).click(function() {				
				hideOrShow();
				$('#userName').val(($(this).closest('tr').find('.userName').text()));
				$('#userName').attr('readonly',true);
				$('#name').val(($(this).closest('tr').find('.name').text()));
				$('#name').attr('readonly',true);
				$('#password').val(($(this).closest('tr').find('.password').text()));
				$('#password').attr('readonly',true);
				$('#surname').val(($(this).closest('tr').find('.surname').text()));
				$('#surname').attr('readonly',true);
				$('#userType').val(($(this).closest('tr').find('.userType').text()));
				$('#userType :not(:selected)').attr('disabled','disabled');
				$('#office').val(($(this).closest('tr').find('.office').text()));
				$('#office').attr('readonly',true);
				$('#isBlocked').val(($(this).closest('tr').find('.isBlocked').text()));
				$('#isBlocked').attr('readonly',true)
				$('#isBlocked :not(:selected)').attr('disabled','disabled');
				$('#userSubmit').val("delete")
				$('#userId').val($(this).attr('alt'));
				$('#userForm').attr('action','adminUserView.php?action=delete');
				$('.pOffice').show();
			});

			$( ".userModify" ).click(function() {				
				hideOrShow();
				$('#userName').val(($(this).closest('tr').find('.userName').text()));
				$('#userName').attr('readonly',true);
				$('#name').val(($(this).closest('tr').find('.name').text()));
				$('#name').attr('readonly',false);
				$('#password').val(($(this).closest('tr').find('.password').text()));
				$('#password').attr('readonly',false);
				$('#surname').val(($(this).closest('tr').find('.surname').text()));
				$('#surname').attr('readonly',false);
				$('#userType').val(($(this).closest('tr').find('.userType').text()));
				$('#userType').attr('readonly',true);
				$('#userType :not(:selected)').attr('disabled','disabled');
				$('#office').val(($(this).closest('tr').find('.office').text()));
				$('#office').attr('readonly',true);
				$('#userSubmit').val("modify")
				$('#isBlocked').val(($(this).closest('tr').find('.isBlocked').text()));
				$('#isBlocked').attr('readonly',false);
				$('#isBlocked :not(:selected)').removeAttr('disabled');
				$('#userId').val($(this).attr('alt'));
				$('#userForm').attr('action','adminUserView.php?action=modify');
				$('.pOffice').show();

			});

			$( ".userAdd" ).click(function() {				
				hideOrShow();
				$('#office').hide();
				$('#userName').val(($(this).closest('tr').find('.userName').text()));
				$('#userName').attr('readonly',false);
				$('#name').val(($(this).closest('tr').find('.name').text()));
				$('#name').attr('readonly',false);
				$('#surname').val(($(this).closest('tr').find('.surname').text()));
				$('#surname').attr('readonly',false);
				$('#userType').val(($(this).closest('tr').find('.userType').text()));
				$('#userType').attr('readonly',false);
				$('#userType :not(:selected)').removeAttr('disabled');
				$('#password').val(($(this).closest('tr').find('.password').text()));
				$('#password').attr('readonly',false);
				$('#office').val(($(this).closest('tr').find('.office').text()));
				$('#office').attr('readonly',true);
				$('#isBlocked').val(($(this).closest('tr').find('.isBlocked').text()));
				$('#isBlocked').attr('readonly',false)
				$('#isBlocked :not(:selected)').removeAttr('disabled');
				$('#userSubmit').val("add")
				$('#userForm').attr('action','adminUserView.php?action=add');
				$('.pOffice').hide();
			});

			$( "#userFormContainer" ).hide();
		});
	</script>
	<div class="box_item_center" id="userFormContainer">
		<form id="userForm" class="userForm rounded shadow" action="adminUserView.php" method="post" >
			<p class="pInput">USERNAME</p><input id="userName" class="input" type="text" name="userName" readonly>
			<p class="pInput">PASSWORD</p><input id="password" class="input" type="password" name="password">
			<p class="pInput">NAME</p><input id="name" class="input" type="text" name="name">
			<p class="pInput">SURNAME</p><input id="surname" class="input" type="text" name="surname">
			<p class="pInput">ROLE</p><select id="userType" name="role" class="input">
				<option value="admin">admin</option>
				<option value="regular">regular</option>
			</select>
			<p class="pInput">LOCKED</p><select id="isBlocked" name="isBlocked" class="input">
				<option value="UNLOCKED">UNLOCKED</option>
				<option value="LOCKED">LOCKED</option>
			</select>
			<p class="pInput pOffice">OFFICE</p><input id="office" class="input pOffice" type="text" name="office">
			<p class="pSubmit">&nbsp;</p><input id="userSubmit" class="inputSubmit " type="submit" value="submit">
			<p class="pInput" style="display:none;"></p><input id="userId" class="input" style="display:none;" type="hidden" name="userId">
		</form>
	</div>
</div>

<?php 
	include_once("./php/footer.php");
?>