<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	include_once("./utils/Validator.php");
	$val = SecurityCheck::checkIdentity("regular");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	include_once("./php/header.php");

	require_once('./dao/generated/include_dao.php');

	if(isset($_GET['action'])){
		if($_GET['action'] == "votecast"){

			if(isset($_POST['electionId']) && isset($_SESSION['userId']) && isset($_POST['candidateId']) && isset($_POST['officeId'])
				 && !empty($_POST['electionId']) && !empty($_SESSION['userId']) && !empty($_POST['candidateId']) && !empty($_POST['officeId'])){
				try{

					$voteList = DAOFactory::getVoteDAO()->queryAlreadyCastedVote($_SESSION['userId'],$_POST['electionId']);
					if(sizeof($voteList)>0){
						throw new Exception("You cannot cast your vote twice. Do not force a refresh on the page");
					}

					$transaction = new Transaction();
					$vote 	= new Vote();
					$recap 	= new Recap();

					$vote->voterId 		= $_SESSION['userId'];
					$vote->electionId 	= $_POST['electionId'];
					DAOFactory::getVoteDAO()->insert($vote);

					//check if a votes'pool for the choosen candidate already exists
					$recapList = DAOFactory::getRecapDAO()->queryGetElectionPool($_POST['electionId'],$_POST['candidateId']);
					if(sizeof($recapList)==1){
						$recap = $recapList[0];
						$recap->qty = $recap->qty + 1;

						DAOFactory::getRecapDAO()->update($recap);
					}else if (sizeof($recapList)==0){
						$recap->candidateId 	= $_POST['candidateId'];
						$recap->electionId 		= $_POST['electionId'];
						$recap->officeId 		= $_POST['officeId'];
						$recap->qty 			= 1;

						DAOFactory::getRecapDAO()->insert($recap);
					}else{
						throw new Exception("An error occured while registering your vote");
					}

					$transaction->commit();	
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}
		}
	}
	
?>


<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./regular.php">REGULAR</a></div>
	<div class="nav_active">ELECTION</div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>

<div class="table_row">
	<div class="box_item_center">
		<div class="table_wrapper">
			<div class="nano">
				<div class="content">
					<table>
						<thead>
							<tr>
								<th>Office</th>
								<th>Capacity <?php echo '(years)'; ?></th>
								<th>Description</th>
								<th>Start</th>
								<th>End</th>
								<th>Candidates</th>
								<th>Cast a vote</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$electionList = DAOFactory::getElectionDAO()->queryValidOnes($_SESSION['userId']);

								if(count($electionList) > 0){
									foreach ($electionList as $election){
										$office = DAOFactory::getOfficeDAO()->load($election->officeId);

										echo '<tr class="officeData">';
										echo '<td class="officeTitle" title="'.$office->officeId.'">'.$office->title."</td>";
										echo '<td class="capacity">'.intval(Validator::day2Range($office->capacity, 'year')).'</td>';
										echo '<td class="description">'.$election->description.'</td>';
										echo '<td class="start">'.date("d/m/Y", strtotime($election->startingDate)).'</td>';
										echo '<td class="end">'.date("d/m/Y", strtotime($election->endingDate)).'</td>';
										echo '<td class="choice">';
											echo '<select class="electionChoice">';
												echo '<option value="-1"></option>';
												$candidateList = DAOFactory::getCandidateDAO()->queryByElectionId($election->electionId);

												foreach ($candidateList as $candidate) {
													$user = DAOFactory::getUserDAO()->load($candidate->userCandidateId);

													if(isset($user)&&!empty($user)){
														echo '<option value="'.$user->userId.'">'.$user->name.' '.$user->surname.'</option>';
													}
												}

											echo '</select>';
										echo'</td>';
										echo '<td>';
										echo '<img class="candidateAction" src="./grapx/edit.png" alt="'.$election->electionId.'" width="20px">';
										echo'</td>';
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog" title="candidate's choice">You need to choose one valid candidate to cast your vote.</div>

<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#candidateFormContainer").is(":visible")){
					// $( "#candidateForm" ).hide(400);
				}else{
					$( "#candidateFormContainer" ).show(400);
				}
			};

			$( ".candidateAction" ).click(function() {
				if($(this).closest('tr').find(".electionChoice").find(':selected').val() == -1){
					$('#dialog').dialog();
					$( "#candidateFormContainer" ).hide();
				}else{
					$("#dialog").hide();
					hideOrShow();
					$('#title').val(($(this).closest('tr').find('.officeTitle').text()));
					$('#capacity').val(($(this).closest('tr').find('.capacity').text()));
					$('#description').html(($(this).closest('tr').find('.description').text()));
					$('#officeSubmit').val("cast a vote")
					$('#electionId').val($(this).attr('alt'));
					$('#officeId').val($(this).closest('tr').find('.officeTitle').attr('title'));
					$('#candidateId').val($(this).closest('tr').find(".electionChoice").find(':selected').val());
					$('#candidateName').val($(this).closest('tr').find(".electionChoice").find(':selected').text());
					$('#candidateForm').attr('action','regularElectionView.php?action=votecast');
				}
			});

			$( "#candidateFormContainer" ).hide();
			$("#dialog").hide();
		});
	</script>
	<div class="box_item_center" id="candidateFormContainer">
		<form id="candidateForm" class="candidateForm rounded shadow" action="adminOfficeView.php" method="post" >
			<p class="pInput">TITLE</p><input id="title" class="input" readonly="readonly">
			<p class="pInput">CAPACITY</p><input id="capacity" class="input" readonly="readonly">
			<p class="pInput"  style="vertical-align:top">CANDIDATE</p><input id="candidateName" class="input" readonly="readonly"  style="vertical-align:top">
			<p class="pInput" style="vertical-align:top">DESCRIPTION</p><textarea id="description" class="input" style="resize: none;" readonly="readonly" cols="50" rows="5"></textarea>
			
			<p class="pSubmit">&nbsp;</p><input id="officeSubmit" class="inputSubmit" type="submit" value="submit">
			<p class="pInput" style="display:none;"></p><input id="electionId" class="input" style="display:none;" type="hidden" name="electionId">
			<p class="pInput" style="display:none;"></p><input id="candidateId" class="input" style="display:none;" type="hidden" name="candidateId">
			<p class="pInput" style="display:none;"></p><input id="officeId" class="input" style="display:none;" type="hidden" name="officeId">			
		</form>
	</div>
</div>

<?php 
	include_once("./php/footer.php");
?>