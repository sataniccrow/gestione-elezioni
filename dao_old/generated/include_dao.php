<?php
	//include all DAO files
	require_once('class/sql/Connection.class.php');
	require_once('class/sql/ConnectionFactory.class.php');
	require_once('class/sql/ConnectionProperty.class.php');
	require_once('class/sql/QueryExecutor.class.php');
	require_once('class/sql/Transaction.class.php');
	require_once('class/sql/SqlQuery.class.php');
	require_once('class/core/ArrayList.class.php');
	require_once('class/dao/DAOFactory.class.php');
 	
	require_once('class/dao/CandidateDAO.class.php');
	require_once('class/dto/Candidate.class.php');
	require_once('class/mysql/CandidateMySqlDAO.class.php');
	require_once('class/mysql/ext/CandidateMySqlExtDAO.class.php');
	require_once('class/dao/ElectionDAO.class.php');
	require_once('class/dto/Election.class.php');
	require_once('class/mysql/ElectionMySqlDAO.class.php');
	require_once('class/mysql/ext/ElectionMySqlExtDAO.class.php');
	require_once('class/dao/OfficeDAO.class.php');
	require_once('class/dto/Office.class.php');
	require_once('class/mysql/OfficeMySqlDAO.class.php');
	require_once('class/mysql/ext/OfficeMySqlExtDAO.class.php');
	require_once('class/dao/UserDAO.class.php');
	require_once('class/dto/User.class.php');
	require_once('class/mysql/UserMySqlDAO.class.php');
	require_once('class/mysql/ext/UserMySqlExtDAO.class.php');
	require_once('class/dao/VoteDAO.class.php');
	require_once('class/dto/Vote.class.php');
	require_once('class/mysql/VoteMySqlDAO.class.php');
	require_once('class/mysql/ext/VoteMySqlExtDAO.class.php');
	require_once('class/dao/UtilityDAO.class.php');
	require_once('class/mysql/UtilityMySqlDAO.class.php');
	require_once('class/mysql/ext/UtilityMySqlExtDAO.class.php');

?>