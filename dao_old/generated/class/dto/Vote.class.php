<?php
	/**
	 * Object represents table 'vote'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2013-08-02 16:37	 
	 */
	class Vote{
		
		var $voteId;
		var $voterId;
		var $candidateId;
		var $electionId;
		var $isBlank;
		
	}
?>