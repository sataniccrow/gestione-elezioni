<?php
	/**
	 * Object represents table 'user'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2013-08-02 16:37	 
	 */
	class User{
		
		var $userId;
		var $userType;
		var $officeId;
		var $userName;
		var $password;
		var $name;
		var $surname;
		var $startingDate;
		var $endingDate;
		
	}
?>