<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
interface ElectionDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Election 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param election primary key
 	 */
	public function delete($electionId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Election election
 	 */
	public function insert($election);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Election election
 	 */
	public function update($election);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByStartingDate($value);

	public function queryByEndingDate($value);

	public function queryByDescription($value);

	public function queryByOfficeId($value);


	public function deleteByStartingDate($value);

	public function deleteByEndingDate($value);

	public function deleteByDescription($value);

	public function deleteByOfficeId($value);


}
?>