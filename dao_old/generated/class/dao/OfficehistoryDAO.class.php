<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
interface OfficehistoryDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Officehistory 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param officehistory primary key
 	 */
	public function delete($officeHistoryId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Officehistory officehistory
 	 */
	public function insert($officehistory);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Officehistory officehistory
 	 */
	public function update($officehistory);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByUserId($value);

	public function queryByOfficeId($value);

	public function queryByElectionId($value);


	public function deleteByUserId($value);

	public function deleteByOfficeId($value);

	public function deleteByElectionId($value);


}
?>