<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
interface VoteDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Vote 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param vote primary key
 	 */
	public function delete($voteId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Vote vote
 	 */
	public function insert($vote);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Vote vote
 	 */
	public function update($vote);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByVoterId($value);

	public function queryByCandidateId($value);

	public function queryByElectionId($value);

	public function queryByIsBlank($value);


	public function deleteByVoterId($value);

	public function deleteByElectionId($value);


}
?>