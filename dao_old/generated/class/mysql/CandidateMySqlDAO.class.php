<?php
/**
 * Class that operate on table 'candidate'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
class CandidateMySqlDAO implements CandidateDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return CandidateMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM candidate WHERE candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM candidate';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM candidate ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param candidate primary key
 	 */
	public function delete($candidateId){
		$sql = 'DELETE FROM candidate WHERE candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($candidateId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param CandidateMySql candidate
 	 */
	public function insert($candidate){
		$sql = 'INSERT INTO candidate (userCandidateId, electionId) VALUES (?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($candidate->userCandidateId);
		$sqlQuery->setNumber($candidate->electionId);

		$id = $this->executeInsert($sqlQuery);	
		$candidate->candidateId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param CandidateMySql candidate
 	 */
	public function update($candidate){
		$sql = 'UPDATE candidate SET userCandidateId = ?, electionId = ? WHERE candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($candidate->userCandidateId);
		$sqlQuery->setNumber($candidate->electionId);

		$sqlQuery->setNumber($candidate->candidateId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM candidate';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByUserCandidateId($value){
		$sql = 'SELECT * FROM candidate WHERE userCandidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByElectionId($value){
		$sql = 'SELECT * FROM candidate WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByUserCandidateId($value){
		$sql = 'DELETE FROM candidate WHERE userCandidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByElectionId($value){
		$sql = 'DELETE FROM candidate WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return CandidateMySql 
	 */
	protected function readRow($row){
		$candidate = new Candidate();
		
		$candidate->candidateId = $row['candidateId'];
		$candidate->userCandidateId = $row['userCandidateId'];
		$candidate->electionId = $row['electionId'];

		return $candidate;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return CandidateMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>