<?php
/**
 * Class that operate on table 'user'. Database Mysql.
 *
 * @author: giacomo candian
 * @date: 2013-07-18 14:42
 */
class UserMySqlExtDAO extends UserMySqlDAO{

	/**
	 * Get all records from table ordered by field
	 *
	 * @param $userName column userName
	 * @param $userRole column userRole
	 */
	public function queryByUserNameAndRole($userName,$userRole){
		$sql = 'SELECT * FROM user WHERE userName = ? and userType = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($userName);
		$sqlQuery->set($userRole);
		return $this->getList($sqlQuery);
	}
}
?>