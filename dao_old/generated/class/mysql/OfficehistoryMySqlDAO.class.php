<?php
/**
 * Class that operate on table 'officehistory'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
class OfficehistoryMySqlDAO implements OfficehistoryDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return OfficehistoryMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM officehistory WHERE officeHistoryId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM officehistory';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM officehistory ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param officehistory primary key
 	 */
	public function delete($officeHistoryId){
		$sql = 'DELETE FROM officehistory WHERE officeHistoryId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($officeHistoryId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param OfficehistoryMySql officehistory
 	 */
	public function insert($officehistory){
		$sql = 'INSERT INTO officehistory (userId, officeId, electionId) VALUES (?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($officehistory->userId);
		$sqlQuery->setNumber($officehistory->officeId);
		$sqlQuery->setNumber($officehistory->electionId);

		$id = $this->executeInsert($sqlQuery);	
		$officehistory->officeHistoryId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param OfficehistoryMySql officehistory
 	 */
	public function update($officehistory){
		$sql = 'UPDATE officehistory SET userId = ?, officeId = ?, electionId = ? WHERE officeHistoryId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($officehistory->userId);
		$sqlQuery->setNumber($officehistory->officeId);
		$sqlQuery->setNumber($officehistory->electionId);

		$sqlQuery->setNumber($officehistory->officeHistoryId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM officehistory';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByUserId($value){
		$sql = 'SELECT * FROM officehistory WHERE userId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByOfficeId($value){
		$sql = 'SELECT * FROM officehistory WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByElectionId($value){
		$sql = 'SELECT * FROM officehistory WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByUserId($value){
		$sql = 'DELETE FROM officehistory WHERE userId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByOfficeId($value){
		$sql = 'DELETE FROM officehistory WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByElectionId($value){
		$sql = 'DELETE FROM officehistory WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return OfficehistoryMySql 
	 */
	protected function readRow($row){
		$officehistory = new Officehistory();
		
		$officehistory->userId = $row['userId'];
		$officehistory->officeId = $row['officeId'];
		$officehistory->electionId = $row['electionId'];
		$officehistory->officeHistoryId = $row['officeHistoryId'];

		return $officehistory;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return OfficehistoryMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>