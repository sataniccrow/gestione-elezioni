<?php
/**
 * Class that operate on table 'office'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-02 16:37
 */
class OfficeMySqlDAO implements OfficeDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return OfficeMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM office WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM office';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM office ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param office primary key
 	 */
	public function delete($officeId){
		$sql = 'DELETE FROM office WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($officeId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param OfficeMySql office
 	 */
	public function insert($office){
		$sql = 'INSERT INTO office (capacity, description, title) VALUES (?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($office->capacity);
		$sqlQuery->set($office->description);
		$sqlQuery->set($office->title);

		$id = $this->executeInsert($sqlQuery);	
		$office->officeId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param OfficeMySql office
 	 */
	public function update($office){
		$sql = 'UPDATE office SET capacity = ?, description = ?, title = ? WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($office->capacity);
		$sqlQuery->set($office->description);
		$sqlQuery->set($office->title);

		$sqlQuery->setNumber($office->officeId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM office';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByCapacity($value){
		$sql = 'SELECT * FROM office WHERE capacity = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDescription($value){
		$sql = 'SELECT * FROM office WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByTitle($value){
		$sql = 'SELECT * FROM office WHERE title = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByCapacity($value){
		$sql = 'DELETE FROM office WHERE capacity = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDescription($value){
		$sql = 'DELETE FROM office WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByTitle($value){
		$sql = 'DELETE FROM office WHERE title = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return OfficeMySql 
	 */
	protected function readRow($row){
		$office = new Office();
		
		$office->officeId = $row['officeId'];
		$office->capacity = $row['capacity'];
		$office->description = $row['description'];
		$office->title = $row['title'];

		return $office;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return OfficeMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>