<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	include_once("./utils/Validator.php");
	$val = SecurityCheck::checkIdentity("admin");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	include_once("./php/header.php");

	require_once('./dao/generated/include_dao.php');

	if(isset($_GET['action'])){
		if($_GET['action'] == "add"){
			if(isset($_POST['title']) && isset($_POST['capacity'])
				 && !empty($_POST['title']) && !empty($_POST['capacity'])){
				try{
					$transaction = new Transaction();
					$office = new Office();
					
					Validator::validate($_POST['capacity']);

					$office->title = $_POST['title'];
					$office->capacity = Validator::range2Day($_POST['capacity'], 'year');
					$office->description = $_POST['description'];

					DAOFactory::getOfficeDAO()->insert($office);
					$transaction->commit();	
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}
		}

		if($_GET['action'] == 'delete' && isset($_POST['officeId'])){
			$transaction = new Transaction();
			DAOFactory::getOfficeDAO()->delete($_POST['officeId']);
			$transaction->commit();
		}else{
			if($_GET['action'] == 'modify' && isset($_POST['officeId'])){
				
				$office = DAOFactory::getOfficeDAO()->load($_POST['officeId']);
				try{
					if(isset($office)){
						if(Validator::validate($_POST['capacity'])){
							$transaction = new Transaction();
							$office->capacity = (isset($_POST['capacity']) && $_POST['capacity'] != '') ? Validator::range2Day($_POST['capacity'], 'year') : $office->capacity;
							$office->description = (isset($_POST['description']) && $_POST['description'] != '') ? $_POST['description'] : $office->description;

							DAOFactory::getOfficeDAO()->update($office);
							$transaction->commit();	
						}
					}
				}catch(Exception $e){
					echo $e->getMessage();
				}
			}
		}
	}
	
?>


<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./admin.php">ADMIN</a></div>
	<div class="nav_active">OFFICE</div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>

<div class="table_row">
	<div class="box_item_center">
		<div class="table_wrapper">
			<div class="nano">
				<div class="content">
					<table>
						<thead>
							<tr>
								<th>Title</th>
								<th>Capacity <?php echo '(years)'; ?></th>
								<th>Description</th>
								<th>Delete</th>
								<th>Modify</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$officeList = array();
								$officeList = DAOFactory::getOfficeDAO()->queryAllOrderBy("title");

								if(count($officeList) > 0){
									foreach ($officeList as $val){
										echo '<tr class="officeData">';
										echo '<td class="officeTitle">'.$val->title."</td>";
										echo '<td class="capacity">'.intval(Validator::day2Range($val->capacity, 'year')).'</td>';
										echo '<td class="description">'.$val->description.'</td>';
										echo '<td>';
										echo '<img class="officeDelete" src="./grapx/remove.png" alt="'.$val->officeId.'" width="20px">';
										echo'</td>';
										echo '<td>';
										echo '<img class="officeModify" src="./grapx/edit.png" alt="'.$val->officeId.'" width="20px">';
										echo'</td>';
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="table_row_footer">
			<img src="./grapx/add.png" class="officeAdd" alt="add new user" width="40px">
		</div>
	</div>
</div>

<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#officeFormContainer").is(":visible")){
					// $( "#officeForm" ).hide(400);
				}else{
					$( "#officeFormContainer" ).show(400);
				}
			};

			$( ".officeDelete" ).click(function() {				
				hideOrShow();
				$('#title').val(($(this).closest('tr').find('.officeTitle').text()));
				$('#title').attr('readonly',true);
				$('#capacity').val(($(this).closest('tr').find('.capacity').text()));
				$('#capacity').attr('readonly',true);
				$('#description').val(($(this).closest('tr').find('.description').text()));
				$('#description').attr('readonly',true);
				$('#officeSubmit').val("delete")
				$('#officeId').val($(this).attr('alt'));
				$('#officeForm').attr('action','adminOfficeView.php?action=delete');
			});

			$( ".officeModify" ).click(function() {				
				hideOrShow();
				$('#title').val(($(this).closest('tr').find('.officeTitle').text()));
				$('#title').attr('readonly',true);
				$('#capacity').val(($(this).closest('tr').find('.capacity').text()));
				$('#capacity').attr('readonly',false);
				$('#description').val(($(this).closest('tr').find('.description').text()));
				$('#description').attr('readonly',false);
				$('#officeSubmit').val("modify")
				$('#officeId').val($(this).attr('alt'));
				$('#officeForm').attr('action','adminOfficeView.php?action=modify');
			});

			$( ".officeAdd" ).click(function() {				
				hideOrShow();
				$('#title').val(($(this).closest('tr').find('.officeTitle').text()));
				$('#title').attr('readonly',false);
				$('#capacity').val(($(this).closest('tr').find('.capacity').text()));
				$('#capacity').attr('readonly',false);
				$('#description').val(($(this).closest('tr').find('.description').text()));
				$('#description').attr('readonly',false);
				$('#office').attr('readonly',true);
				$('#officeSubmit').val("add")
				$('#officeForm').attr('action','adminOfficeView.php?action=add');
			});



			$( "#officeFormContainer" ).hide();
		});
	</script>
	<div class="box_item_center" id="officeFormContainer">
		<form id="officeForm" class="officeForm rounded shadow" action="adminOfficeView.php" method="post" >
			<p class="pInput">TITLE</p><input id="title" class="input" type="text" name="title">
			<p class="pInput">CAPACITY</p><input id="capacity" class="input" type="text" name="capacity">
			<p class="pInput">DESCRIPTION</p><input id="description" class="input" type="text" name="description">
			<p class="pSubmit">&nbsp;</p><input id="officeSubmit" class="inputSubmit" type="submit" value="submit">
			<p class="pInput" style="display:none;"></p><input id="officeId" class="input" style="display:none;" type="hidden" name="officeId">
		</form>
	</div>
</div>

<?php 
	include_once("./php/footer.php");
?>