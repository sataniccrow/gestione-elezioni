<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	$val = SecurityCheck::checkIdentity("regular");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	include_once("./php/header.php");
?>
<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./admin.php">REGULAR</a></div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>
<div class="table_row">
	<div class="box_item_center" style="vertical-align:middle">
			<div class="nav_active" style="display:block; margin:10px auto; width:300px"><a href="regularCandidateView.php" style="color:white">CANDIDATE YOURSELF</a></div>
			<div class="nav_active" style="display:block; margin:10px auto; width:300px"><a href="regularElectionView.php"style="color:white">VOTE CURRENT ELECTION(S)</a></div>
		</div>
</div>

<?php 
	include_once("./php/footer.php");
?>