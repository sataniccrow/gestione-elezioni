CREATE DATABASE  IF NOT EXISTS `gestione_elezioni` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gestione_elezioni`;
-- MySQL dump 10.13  Distrib 5.1.40, for Win32 (ia32)
--
-- Host: localhost    Database: gestione_elezioni
-- ------------------------------------------------------
-- Server version	5.5.8

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `electionId` int(11) NOT NULL AUTO_INCREMENT,
  `startingDate` date NOT NULL,
  `endingDate` date NOT NULL,
  `description` varchar(45) NOT NULL,
  `officeId` int(11) NOT NULL,
  PRIMARY KEY (`electionId`),
  KEY `office` (`officeId`),
  CONSTRAINT `office` FOREIGN KEY (`officeId`) REFERENCES `office` (`officeId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (4,'1970-01-01','1970-01-05','elezioni anticipate!',10),(24,'2013-10-29','2013-11-29','test datepicker',4),(25,'2013-08-01','2013-08-10','nuove elezioni',8),(26,'2013-08-20','2013-09-30','verifica attuale1',4),(27,'2013-08-20','2013-10-25','verifica attuale2',4);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recap`
--

DROP TABLE IF EXISTS `recap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recap` (
  `votesId` int(11) NOT NULL AUTO_INCREMENT,
  `electionId` int(11) NOT NULL,
  `officeId` int(11) NOT NULL,
  `candidateId` int(11) NOT NULL,
  `qty` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`votesId`),
  KEY `votestocandidate` (`candidateId`),
  KEY `votestoelection` (`electionId`),
  KEY `votestooffice` (`officeId`),
  CONSTRAINT `votestocandidate` FOREIGN KEY (`candidateId`) REFERENCES `candidate` (`userCandidateId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `votestoelection` FOREIGN KEY (`electionId`) REFERENCES `election` (`electionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `votestooffice` FOREIGN KEY (`officeId`) REFERENCES `office` (`officeId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recap`
--

LOCK TABLES `recap` WRITE;
/*!40000 ALTER TABLE `recap` DISABLE KEYS */;
INSERT INTO `recap` VALUES (6,26,4,6,3),(7,27,4,8,4);
/*!40000 ALTER TABLE `recap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `office`
--

DROP TABLE IF EXISTS `office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `office` (
  `officeId` int(11) NOT NULL AUTO_INCREMENT,
  `capacity` int(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY (`officeId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `office`
--

LOCK TABLES `office` WRITE;
/*!40000 ALTER TABLE `office` DISABLE KEYS */;
INSERT INTO `office` VALUES (4,36500,'investito del titolo di capo del mondo per un anno intero','capo del mondo'),(5,720,'re del test','re del regno'),(8,365,'azz','re del regno'),(10,730,'il sovrano del pregiudizio ha una caratteristica nota di razzismo che si esplica nella sensazione di odio e fastidio verso qualunque elemento non si conformi ai suoi usi','sovrano del pregiudizio');
/*!40000 ALTER TABLE `office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `candidateId` int(11) NOT NULL AUTO_INCREMENT,
  `userCandidateId` int(11) NOT NULL,
  `electionId` int(11) NOT NULL,
  PRIMARY KEY (`candidateId`),
  KEY `candidateToUser` (`userCandidateId`),
  KEY `candidateToElection` (`electionId`),
  CONSTRAINT `candidateToElection` FOREIGN KEY (`electionId`) REFERENCES `election` (`electionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `candidateToUser` FOREIGN KEY (`userCandidateId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (9,8,27),(17,6,26);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `officehistory`
--

DROP TABLE IF EXISTS `officehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `officehistory` (
  `userId` int(11) NOT NULL,
  `officeId` int(11) NOT NULL,
  `electionId` int(11) NOT NULL,
  `officeHistoryId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`officeHistoryId`),
  KEY `userH` (`userId`),
  KEY `officeH` (`officeId`),
  KEY `electionh` (`electionId`),
  CONSTRAINT `electionh` FOREIGN KEY (`electionId`) REFERENCES `election` (`electionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `officeH` FOREIGN KEY (`officeId`) REFERENCES `office` (`officeId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userH` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `officehistory`
--

LOCK TABLES `officehistory` WRITE;
/*!40000 ALTER TABLE `officehistory` DISABLE KEYS */;
INSERT INTO `officehistory` VALUES (53,10,4,1);
/*!40000 ALTER TABLE `officehistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `voteId` int(11) NOT NULL AUTO_INCREMENT,
  `voterId` int(11) NOT NULL,
  `electionId` int(11) NOT NULL,
  PRIMARY KEY (`voteId`),
  KEY `voter` (`voterId`),
  KEY `election` (`electionId`),
  KEY `voteToUser` (`voterId`),
  KEY `voteToElection` (`electionId`),
  CONSTRAINT `voteToUser` FOREIGN KEY (`voterId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `voteToElection` FOREIGN KEY (`electionId`) REFERENCES `election` (`electionId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote`
--

LOCK TABLES `vote` WRITE;
/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
INSERT INTO `vote` VALUES (8,6,26),(9,6,27),(10,8,26),(11,8,27),(12,8,27),(13,9,26),(14,9,27);
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userType` varchar(45) NOT NULL DEFAULT 'GUEST',
  `officeId` int(11) DEFAULT NULL,
  `userName` varchar(45) NOT NULL,
  `password` varchar(150) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `startingDate` date DEFAULT NULL,
  `endingDate` date DEFAULT NULL,
  `isBlocked` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  KEY `officer` (`officeId`),
  CONSTRAINT `officer` FOREIGN KEY (`officeId`) REFERENCES `office` (`officeId`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'regular',5,'albe1','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto1','Turinetti','2013-08-12','2013-08-19',0),(8,'regular',5,'albe2','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto2','Turinetti','2013-08-12','2013-08-19',0),(9,'regular',5,'albe3','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti','2013-05-06','2013-05-07',0),(10,'regular',NULL,'albe4','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti','0000-00-00','0000-00-00',0),(12,'regular',NULL,'albe6','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti',NULL,NULL,0),(13,'regular',NULL,'albe7','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti',NULL,NULL,0),(14,'regular',NULL,'albe8','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti',NULL,NULL,0),(15,'regular',NULL,'albe9','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti','0000-00-00','0000-00-00',0),(16,'regular',NULL,'albe10','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti',NULL,NULL,0),(17,'regular',NULL,'albe11','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','TurinettiTest',NULL,NULL,0),(28,'regular',NULL,'albe219','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Albert2','Turinetti',NULL,NULL,0),(32,'regular',NULL,'albe2113','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','Alberto','Turinetti',NULL,NULL,0),(53,'admin',8,'albe_admin','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','albe','turi','2013-05-06','2013-05-07',0),(55,'admin',NULL,'testAdmin','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','super','super',NULL,NULL,0),(60,'admin',NULL,'sc','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','albe','albe',NULL,NULL,0),(61,'regular',NULL,'1','ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff','1','1','0000-00-00','0000-00-00',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-22 11:39:45
