<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php 
	include_once("./utils/Validator.php"); 
?>

<?php
	if(isset($_POST["userType"]) && $_POST["userType"]=="guest"){
		//manage global error
		//redirect to index
		$page = "guest.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}else{
		if(!isset($_POST["userName"]))
		{
			$page = "index.php";			
			header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
			exit();
		}else{
			if(isset($_POST["userType"]) && ($_POST["userType"] === "admin" || $_POST["userType"] === "regular")){
				require_once('./dao/generated/include_dao.php');
				$userList = array();
				$userList = DAOFactory::getUserDAO()->queryByUserNameAndRole($_POST["userName"],$_POST["userType"]);
				if(count($userList)>0){
					//set user data
					if(Validator::checkPassword($userList[0]->password,$_POST['password']) && $userList[0]->isBlocked != 1){
						$_SESSION['userName'] 	= $userList[0]->userName;
						$_SESSION['userId']		= $userList[0]->userId;
						$_SESSION['userType'] 	= $userList[0]->userType;
						//redirect to the role manager page
						//$page = "/".$_POST["userType"].".php";
						$page = ($_POST["userType"] === "admin")?"admin.php":"regular.php";
						header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
						exit();
					}else{
						//managed error using GET error variable
						//redirect to index
						$errorType = (Validator::checkPassword($userList[0]->password,$_POST['password']))?"blockedUser":"wrongPassword";
						$page = "index.php";
						header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page.'?error='.$errorType);
						exit();	
					}
				}else{
					//redirect to index
					$errorType = "invalidUser";
					$page = "index.php";
					header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page.'?error='.$errorType);
					exit();					
				}	
			}
		}
	}
?>