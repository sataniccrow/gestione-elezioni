<?php
	if(isset($_GET['error']) && !empty($_GET['error'])){
		
		$errorMessage = '';
		switch ($_GET['error']) {
			case 'blockedUser':
				$errorMessage = 'Your account has been blocked, contact an administrator';
				break;
			case 'wrongPassword':
				$errorMessage = 'The password you have typed does not match';
				break;
			case 'invalidUser':
				$errorMessage = 'The username you have typed is not correct or you have selected a wrong section';
				break;
			default:
				$errorMessage = '';
				break;
		}

		if(!empty($errorMessage)){
			echo $errorMessage;
		}
	}
?>
<div class="table_row">
	<div class="box_item_center">
		<div id="regUserButton" class="item" style="background:url('./grapx/regUser.png')">
			<div class="itemText">USER</div>
		</div>
		<div id="adminButton" class="item" style="background:url('./grapx/admin.png')">
			<div class="itemText">ADMIN</div>
		</div>
		<div id="guestButton" class="item" style="background:url('./grapx/guest.png')">
			<div class="itemText">GUEST</div>
		</div>
	</div>
</div>
<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#loginFormContainer").is(":visible")){
					// $( "#loginForm" ).hide(400);
				}else{
					$( "#loginFormContainer" ).show(400);
				}

				if(text != ""){
					if(text == "regular"){
						$("#loginType").html("-- regular access --")
						$("#hiddenLoginType").val(text)

					}else{
						if(text == "admin"){
							$("#loginType").html("-- admin access --")
							$("#hiddenLoginType").val(text)
						}
					}
				}
			};

			$( "#regUserButton" ).click(function() {
				hideOrShow("regular")
			});

			$( "#adminButton" ).click(function() {
				hideOrShow("admin")
			});

			$( "#guestButton" ).click(function() {
				$("#hiddenLoginType").val("guest")
				$("#loginForm").submit()
			});

			$( "#loginFormContainer" ).hide();
		});
	</script>
	<div class="box_item_center" id="loginFormContainer">
		<form id="loginForm" class="form rounded shadow" action="roleRedirect.php" method="post" >
			<p class="pInput">LOGIN</p><input class="input" type="text" name="userName">
			<p class="pInput">PASSWORD</p><input class="input" type="password" name="password">
			<p class="pSubmit">&nbsp;</p><input class="inputSubmit" type="submit" value="submit">
			<input id="hiddenLoginType" type="hidden" name="userType" value="regular">
			<p class="pType" id="loginType">- admin -</p>
		</form>
	</div>
</div>