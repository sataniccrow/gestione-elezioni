<?php
	/**
	 * Object represents table 'office'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2013-08-19 15:20	 
	 */
	class Office{
		
		var $officeId;
		var $capacity;
		var $description;
		var $title;
		
	}
?>