<?php
	/**
	 * Object represents table 'election'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2013-08-19 15:20	 
	 */
	class Election{
		
		var $electionId;
		var $startingDate;
		var $endingDate;
		var $description;
		var $officeId;
		
	}
?>