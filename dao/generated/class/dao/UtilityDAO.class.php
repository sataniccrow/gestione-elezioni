<?php
/**
 * Intreface DAO
 *
 * @author: Giacomo Candian
 * @date: 2013-08-01 17:48
 */
interface UtilityDAO{

	/**
	 * Get Valid Office for a specified User
	 *
	 *
	 * @param String $userId
	 * @Return Office[] 
	 */
	public function getValidElectionForUser($userid);

}
?>