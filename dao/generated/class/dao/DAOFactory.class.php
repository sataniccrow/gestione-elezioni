<?php

/**
 * DAOFactory
 * @author: http://phpdao.com
 * @date: ${date}
 */
class DAOFactory{
	
	/**
	 * @return CandidateDAO
	 */
	public static function getCandidateDAO(){
		return new CandidateMySqlExtDAO();
	}

	/**
	 * @return ElectionDAO
	 */
	public static function getElectionDAO(){
		return new ElectionMySqlExtDAO();
	}

	/**
	 * @return OfficeDAO
	 */
	public static function getOfficeDAO(){
		return new OfficeMySqlExtDAO();
	}

	/**
	 * @return OfficehistoryDAO
	 */
	public static function getOfficehistoryDAO(){
		return new OfficehistoryMySqlExtDAO();
	}

	/**
	 * @return RecapDAO
	 */
	public static function getRecapDAO(){
		return new RecapMySqlExtDAO();
	}

	/**
	 * @return UserDAO
	 */
	public static function getUserDAO(){
		return new UserMySqlExtDAO();
	}

	/**
	 * @return VoteDAO
	 */
	public static function getVoteDAO(){
		return new VoteMySqlExtDAO();
	}

	/**
	 * @return UtilityDAO
	 */
	public static function getUtilityDAO(){
		return new UtilityMySqlExtDAO();
	}

}
?>