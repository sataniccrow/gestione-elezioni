<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
interface RecapDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Recap 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param recap primary key
 	 */
	public function delete($votesId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Recap recap
 	 */
	public function insert($recap);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Recap recap
 	 */
	public function update($recap);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByElectionId($value);

	public function queryByOfficeId($value);

	public function queryByCandidateId($value);

	public function queryByQty($value);


	public function deleteByElectionId($value);

	public function deleteByOfficeId($value);

	public function deleteByCandidateId($value);

	public function deleteByQty($value);


}
?>