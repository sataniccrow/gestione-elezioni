<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
interface UserDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return User 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param user primary key
 	 */
	public function delete($userId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param User user
 	 */
	public function insert($user);
	
	/**
 	 * Update record in table
 	 *
 	 * @param User user
 	 */
	public function update($user);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByUserType($value);

	public function queryByOfficeId($value);

	public function queryByUserName($value);

	public function queryByPassword($value);

	public function queryByName($value);

	public function queryBySurname($value);

	public function queryByStartingDate($value);

	public function queryByEndingDate($value);


	public function deleteByUserType($value);

	public function deleteByOfficeId($value);

	public function deleteByUserName($value);

	public function deleteByPassword($value);

	public function deleteByName($value);

	public function deleteBySurname($value);

	public function deleteByStartingDate($value);

	public function deleteByEndingDate($value);


}
?>