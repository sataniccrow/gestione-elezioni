<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
interface OfficeDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Office 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param office primary key
 	 */
	public function delete($officeId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Office office
 	 */
	public function insert($office);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Office office
 	 */
	public function update($office);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByCapacity($value);

	public function queryByDescription($value);

	public function queryByTitle($value);


	public function deleteByCapacity($value);

	public function deleteByDescription($value);

	public function deleteByTitle($value);


}
?>