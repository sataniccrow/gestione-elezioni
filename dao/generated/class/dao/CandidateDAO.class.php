<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
interface CandidateDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Candidate 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param candidate primary key
 	 */
	public function delete($candidateId);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Candidate candidate
 	 */
	public function insert($candidate);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Candidate candidate
 	 */
	public function update($candidate);	

	/**
	 * Delete all rows
	 */
	public function clean();

	public function queryByUserCandidateId($value);

	public function queryByElectionId($value);


	public function deleteByUserCandidateId($value);

	public function deleteByElectionId($value);


}
?>