<?php
/**
 * Class that operate on table 'vote'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
class VoteMySqlDAO implements VoteDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return VoteMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM vote WHERE voteId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM vote';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM vote ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param vote primary key
 	 */
	public function delete($voteId){
		$sql = 'DELETE FROM vote WHERE voteId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($voteId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param VoteMySql vote
 	 */
	public function insert($vote){
		$sql = 'INSERT INTO vote (voterId, electionId) VALUES (?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($vote->voterId);
		$sqlQuery->setNumber($vote->electionId);

		$id = $this->executeInsert($sqlQuery);	
		$vote->voteId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param VoteMySql vote
 	 */
	public function update($vote){
		$sql = 'UPDATE vote SET voterId = ?, electionId = ? WHERE voteId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($vote->voterId);
		$sqlQuery->setNumber($vote->electionId);

		$sqlQuery->setNumber($vote->voteId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM vote';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByVoterId($value){
		$sql = 'SELECT * FROM vote WHERE voterId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByElectionId($value){
		$sql = 'SELECT * FROM vote WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByVoterId($value){
		$sql = 'DELETE FROM vote WHERE voterId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByElectionId($value){
		$sql = 'DELETE FROM vote WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return VoteMySql 
	 */
	protected function readRow($row){
		$vote = new Vote();
		
		$vote->voteId = $row['voteId'];
		$vote->voterId = $row['voterId'];
		$vote->electionId = $row['electionId'];

		return $vote;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return VoteMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>