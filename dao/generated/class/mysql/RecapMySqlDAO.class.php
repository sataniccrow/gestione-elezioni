<?php
/**
 * Class that operate on table 'recap'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
class RecapMySqlDAO implements RecapDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return RecapMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM recap WHERE votesId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM recap';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM recap ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param recap primary key
 	 */
	public function delete($votesId){
		$sql = 'DELETE FROM recap WHERE votesId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($votesId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param RecapMySql recap
 	 */
	public function insert($recap){
		$sql = 'INSERT INTO recap (electionId, officeId, candidateId, qty) VALUES (?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($recap->electionId);
		$sqlQuery->setNumber($recap->officeId);
		$sqlQuery->setNumber($recap->candidateId);
		$sqlQuery->setNumber($recap->qty);
		echo $sqlQuery->getQuery();
		$id = $this->executeInsert($sqlQuery);	
		$recap->votesId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param RecapMySql recap
 	 */
	public function update($recap){
		$sql = 'UPDATE recap SET electionId = ?, officeId = ?, candidateId = ?, qty = ? WHERE votesId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($recap->electionId);
		$sqlQuery->setNumber($recap->officeId);
		$sqlQuery->setNumber($recap->candidateId);
		$sqlQuery->setNumber($recap->qty);

		$sqlQuery->setNumber($recap->votesId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM recap';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByElectionId($value){
		$sql = 'SELECT * FROM recap WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByOfficeId($value){
		$sql = 'SELECT * FROM recap WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByCandidateId($value){
		$sql = 'SELECT * FROM recap WHERE candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByQty($value){
		$sql = 'SELECT * FROM recap WHERE qty = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByElectionId($value){
		$sql = 'DELETE FROM recap WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByOfficeId($value){
		$sql = 'DELETE FROM recap WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByCandidateId($value){
		$sql = 'DELETE FROM recap WHERE candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByQty($value){
		$sql = 'DELETE FROM recap WHERE qty = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return RecapMySql 
	 */
	protected function readRow($row){
		$recap = new Recap();
		
		$recap->votesId = $row['votesId'];
		$recap->electionId = $row['electionId'];
		$recap->officeId = $row['officeId'];
		$recap->candidateId = $row['candidateId'];
		$recap->qty = $row['qty'];

		return $recap;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return RecapMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>