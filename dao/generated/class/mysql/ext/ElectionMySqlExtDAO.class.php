<?php
/**
 * Class that operate on table 'election'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:19
 */
class ElectionMySqlExtDAO extends ElectionMySqlDAO{

	/**
	 * Get all valid records from table
	 * @param Number $voterId
	 * @Return Election[] 
	 */
	public function queryValidOnes($voterId){
		$sql = 'SELECT * FROM election WHERE CURDATE() BETWEEN startingDate AND endingDate and  election.electionId not in (COALESCE((SELECT vote.electionId FROM vote where voterId =?),-1))';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($voterId);

		return $this->getList($sqlQuery);
	}
}
?>