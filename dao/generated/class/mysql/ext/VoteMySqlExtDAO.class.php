<?php
/**
 * Class that operate on table 'vote'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:19
 */
class VoteMySqlExtDAO extends VoteMySqlDAO{

	/**
	 * Return the record if a user already casted a vote for a particular election
	 * @param Number $voterId
	 * @param Number $electionId
	 * @Return Vote[] 
	 */
	public function queryAlreadyCastedVote($voterId,$electionId){
		$sql = 'SELECT * FROM vote WHERE voterId = ? and electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($voterId);
		$sqlQuery->setNumber($electionId);

		return $this->getList($sqlQuery);
	}
	
}
?>