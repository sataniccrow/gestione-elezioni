<?php
/**
 * Class that operate on table 'recap'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:19
 */
class RecapMySqlExtDAO extends RecapMySqlDAO{

	/**
	 * Get all valid records from table
	 * @param Number $candidateId
	 * @param Number $electionId
	 * @Return Recap[] 
	 */
	public function queryGetElectionPool($electionId,$candidateId){
		$sql = 'SELECT * FROM recap WHERE electionId = ? and candidateId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($electionId);
		$sqlQuery->setNumber($candidateId);

		return $this->getList($sqlQuery);
	}
}
?>