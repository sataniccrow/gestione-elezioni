<?php
/**
 * Class that operate on table 'election'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-08-19 15:20
 */
class ElectionMySqlDAO implements ElectionDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return ElectionMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM election WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM election';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM election ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param election primary key
 	 */
	public function delete($electionId){
		$sql = 'DELETE FROM election WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($electionId);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param ElectionMySql election
 	 */
	public function insert($election){
		$sql = 'INSERT INTO election (startingDate, endingDate, description, officeId) VALUES (?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($election->startingDate);
		$sqlQuery->set($election->endingDate);
		$sqlQuery->set($election->description);
		$sqlQuery->setNumber($election->officeId);

		$id = $this->executeInsert($sqlQuery);	
		$election->electionId = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param ElectionMySql election
 	 */
	public function update($election){
		$sql = 'UPDATE election SET startingDate = ?, endingDate = ?, description = ?, officeId = ? WHERE electionId = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($election->startingDate);
		$sqlQuery->set($election->endingDate);
		$sqlQuery->set($election->description);
		$sqlQuery->setNumber($election->officeId);

		$sqlQuery->setNumber($election->electionId);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM election';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

	public function queryByStartingDate($value){
		$sql = 'SELECT * FROM election WHERE startingDate = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByEndingDate($value){
		$sql = 'SELECT * FROM election WHERE endingDate = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDescription($value){
		$sql = 'SELECT * FROM election WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByOfficeId($value){
		$sql = 'SELECT * FROM election WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


	public function deleteByStartingDate($value){
		$sql = 'DELETE FROM election WHERE startingDate = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByEndingDate($value){
		$sql = 'DELETE FROM election WHERE endingDate = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDescription($value){
		$sql = 'DELETE FROM election WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByOfficeId($value){
		$sql = 'DELETE FROM election WHERE officeId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return ElectionMySql 
	 */
	protected function readRow($row){
		$election = new Election();
		
		$election->electionId = $row['electionId'];
		$election->startingDate = $row['startingDate'];
		$election->endingDate = $row['endingDate'];
		$election->description = $row['description'];
		$election->officeId = $row['officeId'];

		return $election;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return ElectionMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>