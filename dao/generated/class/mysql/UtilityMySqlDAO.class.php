<?php
/**
 * Class that operate on table 'office'. Database Mysql.
 *
 * @author: Giacomo Candian
 * @date: 2013-08-01 17:48
 */
class UtilityMySqlDAO implements UtilityDAO{

	/**
	 * Get Valid Office for a specified User
	 *
	 *
	 * @param String $userName
	 * @Return Office[] 
	 */
	public function getValidElectionForUser($userId){
		$sql = 'SELECT * FROM election WHERE election.officeId not in (COALESCE((SELECT user.officeId FROM user where userId =?),-1)) and election.officeId NOT in (COALESCE((SELECT officeHistory.officeId FROM officeHistory where userId =?),-1)) and election.electionId NOT IN (COALESCE((SELECT candidate.electionId  FROM candidate where userCandidateId = ?),-1)) and CURDATE() < election.startingDate';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($userId);
		$sqlQuery->setNumber($userId);
		$sqlQuery->setNumber($userId);
		// echo $sqlQuery->getQuery();
		return $this->getElectionList($sqlQuery);
	}

	protected function getList($sqlQuery){
		echo $sqlQuery->getQuery() .'<br>';

		$tab = QueryExecutor::execute($sqlQuery);
		return $tab;
	}
	
	protected function readElectionRow($row){
		$election = new Election();
		
		$election->electionId = $row['electionId'];
		$election->startingDate = $row['startingDate'];
		$election->endingDate = $row['endingDate'];
		$election->description = $row['description'];
		$election->officeId = $row['officeId'];

		return $election;
	}
	
	protected function getElectionList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readElectionRow($tab[$i]);
		}
		return $ret;
	}
}
?>