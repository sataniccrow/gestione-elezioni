<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	$val = SecurityCheck::checkIdentity("admin");

	include_once("./utils/Validator.php");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	
	include_once("./php/header.php");

	require_once('./dao/generated/include_dao.php');

	if(isset($_GET['action'])){
		if($_GET['action'] == "add"){
			if(isset($_POST['description']) && isset($_POST['startingDate']) && isset($_POST['endingDate']) && isset($_POST['officeId'])
				 && !empty($_POST['description']) && !empty($_POST['startingDate']) && !empty($_POST['endingDate']) && !empty($_POST['officeId'])){
				try{
					$transaction 				= new Transaction();
					$election 					= new Election();
					$election->description 		= $_POST['description'];
					$election->startingDate 	= Validator::string2MysqlDate($_POST['startingDate']);
					$election->endingDate 		= Validator::string2MysqlDate($_POST['endingDate']);
					$election->officeId 		= $_POST['officeId'];

					if(!Validator::checkDate($election->startingDate,$election->endingDate,"Y-m-d")){
						throw new Exception("The election cannot start after its end");
					}

					DAOFactory::getElectionDAO()->insert($election);
					$transaction->commit();
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}else{
				echo 'A mandatory parameter is missing';
			}
		}

		if($_GET['action'] == 'delete' && isset($_POST['electionId'])){
			$transaction = new Transaction();
			DAOFactory::getElectionDAO()->delete($_POST['electionId']);
			$transaction->commit();
		}else{
			if($_GET['action'] == 'modify' && isset($_POST['electionId'])){
				try{
					$election = DAOFactory::getElectionDAO()->load($_POST['electionId']);
					
					if(isset($election)){
						$transaction = new Transaction();
						$election->description 		= (isset($_POST['description']) && $_POST['description'] != '') ? $_POST['description'] : $election->description;
						$election->endingDate 		= (isset($_POST['endingDate']) && $_POST['endingDate'] != '') ? Validator::string2MysqlDate($_POST['endingDate']) : $election->endingDate;
						$election->startingDate 	= (isset($_POST['startingDate']) && $_POST['startingDate'] != '') ? Validator::string2MysqlDate($_POST['startingDate']) : $election->startingDate;

						if(!Validator::checkDate($election->startingDate,$election->endingDate,"Y-m-d")){
							throw new Exception("The election cannot start after its end");
						}

						DAOFactory::getElectionDAO()->update($election);
						$transaction->commit();
					}
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}
		}
	}
	
?>


<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./admin.php">ADMIN</a></div>
	<div class="nav_active">ELECTION</div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>

<div class="table_row">
	<div class="box_item_center">
		<div class="table_wrapper">
			<div class="nano">
				<div class="content">
					<table>
						<thead>
							<tr>
								<th>Description</th>
								<th>Office</th>
								<th>Starts</th>
								<th>Ends</th>
								<th>Delete</th>
								<th>Modify</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$electionList = array();
								$electionList = DAOFactory::getElectionDAO()->queryAllOrderBy("startingDate");

								if(count($electionList) > 0){

									foreach ($electionList as $val){
										
										$office = DAOFactory::getOfficeDAO()->load($val->officeId);

										echo '<tr class="electionData">';
										echo '<td class="description">'.$val->description.'</td>';
										echo '<td class="office">'.$office->title.'</td>';
										echo '<td class="startingDate">'.date("d/m/Y", strtotime($val->startingDate))."</td>";
										echo '<td class="endingDate">'.date("d/m/Y", strtotime($val->endingDate)).'</td>';
										echo '<td>';
										echo '<img class="electionDelete" src="./grapx/remove.png" alt="'.$val->electionId.'" width="20px">';
										echo'</td>';
										echo '<td>';
										echo '<img class="electionModify" src="./grapx/edit.png" alt="'.$val->electionId.'" width="20px">';
										echo'</td>';
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="table_row_footer">
			<img src="./grapx/add.png" class="electionAdd" alt="add new user" width="40px">
		</div>
	</div>
</div>

<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#electionFormContainer").is(":visible")){
					// $( "#userForm" ).hide(400);
				}else{
					$( "#electionFormContainer" ).show(400);
				}
			};
			$( "#endingDate" ).datepicker({ dateFormat: "dd/mm/yy" });
    		$( "#startingDate" ).datepicker({ dateFormat: "dd/mm/yy" });

			$( ".electionDelete" ).click(function() {				
				hideOrShow();
				$('#description').val(($(this).closest('tr').find('.description').text()));
				$('#description').attr('readonly',true);
				$('#startingDate').val(($(this).closest('tr').find('.startingDate').text()));
				$('#startingDate').attr('readonly',true);
				$('#officeId').hide();
				$('#pOffice').hide();
				$('#endingDate').val(($(this).closest('tr').find('.endingDate').text()));
				$('#endingDate').attr('readonly',true);
				$('#electionSubmit').val("delete")
				$('#electionId').val($(this).attr('alt'));
				$('#userForm').attr('action','adminElectionView.php?action=delete');
			});

			$( ".electionModify" ).click(function() {				
				hideOrShow();
				$('#description').val(($(this).closest('tr').find('.description').text()));
				$('#description').attr('readonly',false);
				$('#startingDate').val(($(this).closest('tr').find('.startingDate').text()));
				$('#startingDate').attr('readonly',true);
				$('#officeId').hide();
				$('#pOffice').hide();
				$('#endingDate').val(($(this).closest('tr').find('.endingDate').text()));
				$('#endingDate').attr('readonly',false);
				$('#electionSubmit').val("modify")
				$('#electionId').val($(this).attr('alt'));
				$('#userForm').attr('action','adminElectionView.php?action=modify');
			});

			$( ".electionAdd" ).click(function() {				
				hideOrShow();
				$('#officeId').show();
				$('#pOffice').show();
				$('#description').val("");
				$('#description').attr('readonly',false);
				$('#startingDate').val("");
				$('#startingDate').attr('readonly',false);
				$('#endingDate').val("");
				$('#endingDate').attr('readonly',false);
				$('#electionSubmit').val("add")
				$('#userForm').attr('action','adminElectionView.php?action=add');
			});

			$( "#electionFormContainer").hide();
			$( "#pOffice").hide();
			$('#officeId').hide();
		});
	</script>

	<div class="box_item_center" id="electionFormContainer">
		<form id="userForm" class="electionForm rounded shadow" action="adminElectionView.php" method="post" >
			<p class="pInput">DESCRIPTION</p><input id="description" class="input" type="text" name="description" readonly>
			<p class="pInput">START</p><input id="startingDate" class="input" type="text" name="startingDate">
			<p class="pInput">END</p><input id="endingDate" class="input" type="text" name="endingDate" value="06/14/2008">
			<p class="pInput" id="pOffice">OFFICE</p><select id="officeId" name="officeId" class="input">
			<?php
				$officeList = array();
				$officeList = DAOFactory::getOfficeDAO()->queryAll();

				foreach ($officeList as $office) {
					echo '<option value="'.$office->officeId.'">'.$office->title.'</option>';
				}
			?>
			</select>
			<p class="pSubmit">&nbsp;</p><input id="electionSubmit" class="inputSubmit" type="submit" value="submit">
			<p class="pInput" style="display:none;"></p><input id="electionId" class="input" style="display:none;" type="hidden" name="electionId">
		</form>
	</div>
</div>

<?php 
	include_once("./php/footer.php");
?>