<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php 
	include_once("./php/header.php");
?>

<?php
	 //verify if ther's a session active
	 if(isset($_SESSION['userId'], $_SESSION['userName'], $_SESSION['userType'])){
	 	// 3 cases 
	 	switch ($_SESSION['userType']) {
	 		case 'regular':
		 		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$_SESSION['userType'].".php");
				exit();
	 			break;
	 		case 'admin':
		 		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$_SESSION['userType'].".php");
				exit();
	 			break;
	 	}
	 }else{
	 	//if the user is not logged in
	 	include_once("./loginForm.php");
	}						 
?>

<?php 
	include_once("./php/footer.php");
?>