<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	include_once("./utils/SecurityCheck.php");
	include_once("./utils/Validator.php");
	$val = SecurityCheck::checkIdentity("regular");
	
	if (empty($val) || !$val){
		$page = "index.php";
		header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
		exit();
	}
	include_once("./php/header.php");

	require_once('./dao/generated/include_dao.php');

	if(isset($_GET['action'])){
		if($_GET['action'] == "candidate"){

			if(isset($_POST['electionId']) && isset($_SESSION['userId'])
				 && !empty($_POST['electionId']) && !empty($_SESSION['userId'])){
				try{
					$user = DAOFactory::getUserDAO()->load($_SESSION['userId']);
					$election = DAOFactory::getElectionDAO()->load($_POST['electionId']);
					$candidateList = DAOFactory::getCandidateDAO()->queryByUserCandidateId($user->userId);

					//check if the user is in charge of administering an office, if the record exists, it checks the end of the office
					if($user->officeId != null){
						if(!Validator::checkDate($user->endingDate,$election->startingDate,"Y-m-d")){							
							throw new Exception("At the moment of the election, you still will be on charge of administering an office. Your office will end: ". date("d/m/Y", strtotime($user->endingDate)));
						}
					}

					if(sizeof($candidateList)>0){
						throw new Exception("You have already applied to this election");
					}

					$transaction = new Transaction();
					$candidate = new Candidate();
					
					$candidate->electionId = $_POST['electionId'];
					$candidate->userCandidateId = $_SESSION['userId'];

					DAOFactory::getCandidateDAO()->insert($candidate);
					$transaction->commit();	
				}catch (Exception $e){
					echo $e->getMessage();
				}
			}
		}
	}
	
?>


<div class="table_row" style="height:60px;width:100%">
	<div class="nav" ><a href="./regular.php">REGULAR</a></div>
	<div class="nav_active">CANDIDATE</div>
	<div class="logout"><a href="./logout.php" style="color:white">LOGOUT</a></div>
	<div style="clear: both;"></div>
</div>

<div class="table_row">
	<div class="box_item_center">
		<div class="table_wrapper">
			<div class="nano">
				<div class="content">
					<table>
						<thead>
							<tr>
								<th>Office</th>
								<th>Capacity <?php echo '(years)'; ?></th>
								<th>Description</th>
								<th>Start</th>
								<th>End</th>
								<th>Candidate</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$candidateList = DAOFactory::getCandidateDAO()->queryByUserCandidateId($_SESSION['userId']);
								if(sizeof($candidateList)>0){
									//doNothing
								}else{
									$electionList = DAOFactory::getUtilityDAO()->getValidElectionForUser($_SESSION['userId']);

									if(count($electionList) > 0){
										foreach ($electionList as $election){
											$office = DAOFactory::getOfficeDAO()->load($election->officeId);

											echo '<tr class="officeData">';
											echo '<td class="officeTitle">'.$office->title."</td>";
											echo '<td class="capacity">'.intval(Validator::day2Range($office->capacity, 'year')).'</td>';
											echo '<td class="description">'.$election->description.'</td>';
											echo '<td class="start">'.date("d/m/Y", strtotime($election->startingDate)).'</td>';
											echo '<td class="end">'.date("d/m/Y", strtotime($election->endingDate)).'</td>';
											echo '<td>';
											echo '<img class="candidateAction" src="./grapx/edit.png" alt="'.$election->electionId.'" width="20px">';
											echo'</td>';
											echo "</tr>";
										}
									}
								}

							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="table_row">
	<script>
		$(function() {
		// run the currently selected effect
			function hideOrShow(text){
				if($("#candidateFormContainer").is(":visible")){
					// $( "#candidateForm" ).hide(400);
				}else{
					$( "#candidateFormContainer" ).show(400);
				}
			};

			$( ".candidateAction" ).click(function() {				
				hideOrShow();
				$('#title').val(($(this).closest('tr').find('.officeTitle').text()));
				$('#capacity').val(($(this).closest('tr').find('.capacity').text()));
				$('#description').html(($(this).closest('tr').find('.description').text()));
				$('#officeSubmit').val("candidate")
				$('#electionId').val($(this).attr('alt'));
				$('#candidateForm').attr('action','regularCandidateView.php?action=candidate');
			});

			
			$( "#candidateFormContainer" ).hide();
		});
	</script>
	<div class="box_item_center" id="candidateFormContainer">
		<form id="candidateForm" class="candidateForm rounded shadow" action="adminOfficeView.php" method="post" >
			<p class="pInput">TITLE</p><input id="title" class="input" readonly="readonly">
			<p class="pInput">CAPACITY</p><input id="capacity" class="input" readonly="readonly">
			<p class="pInput" style="vertical-align:top">DESCRIPTION</p><textarea id="description" class="input" style="resize: none;" readonly="readonly" cols="50" rows="10"></textarea>
			<p class="pSubmit">&nbsp;</p><input id="officeSubmit" class="inputSubmit" type="submit" value="submit">
			<p class="pInput" style="display:none;"></p><input id="electionId" class="input" style="display:none;" type="hidden" name="electionId">
		</form>
	</div>
</div>

<?php 
	include_once("./php/footer.php");
?>