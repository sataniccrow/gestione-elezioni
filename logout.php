<?php 
	if(!isset($_SESSION)) {session_start();} 
?>

<?php
	//session cleansing
	session_unset();
	session_destroy();
	$_SESSION = array();
	//redirect to index
	$page = "index.php";
	header("Location: http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/\\')."/".$page);
	exit();
?>