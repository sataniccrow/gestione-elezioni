<?php
/**
 * utility for security checks
 *
 * @author: giacomo candian
 * @date: 19.07.2013
 */

class SecurityCheck{

	//check if a logged user already exists
	public static function checkIdentity($userType){
		return (isset($_SESSION["userId"]) && isset($_SESSION["userName"]) && isset($_SESSION["userType"]) && $_SESSION['userType'] === $userType)?true:false;
	}
}

?>