<?php
/**
 * utility for security checks
 *
 * @author: giacomo candian
 * @date: 19.07.2013
 */

class Validator{
	public static function validate($val){
		if(is_numeric($val)){
			return true;
		}else{
			throw new Exception("The field does not contain a number.");
		}
	}

	public static function createHash($password){
		return hash('sha512',$password);
	}

	public static function checkPassword($hashedPassword, $inputPassword){
		return Validator::createHash($inputPassword) === $hashedPassword;
	}

	public static function string2MysqlDate($string){
		return DateTime::createFromFormat("d/m/Y", $string)->format("Y-m-d");
	}

	public static function checkDate($beforeString,$afterString,$format){
		$before = DateTime::createFromFormat($format, $beforeString);
		$after = DateTime::createFromFormat($format, $afterString);
		return (($after->getTimeStamp())-($before->getTimeStamp())>0);
	}
		
	public static function range2Day($time, $format){
		$range = 0;

		switch ($format) {
			case 'day':
				$range = $time;
				break;
			
			case 'month':
				$range = $time*30;
				break;
			
			case 'year':
				$range = $time*365;
				break;
			
			default:
				# day
				$range = $time;
				break;
		}
		return abs($range);
	}

		public static function day2Range($day, $format){
		$range = 0;

		switch ($format) {
			case 'day':
				$range = $day;
				break;
			
			case 'month':
				$range = $day/30;
				break;
			
			case 'year':
				$range = $day/365;
				break;
			
			default:
				# day
				$range = $day;
				break;
		}
		return abs($range);
	}
}

?>